function getCircle(radius) {
    let coordinates = [];
    let inc = 0.1;
    for (let x = -radius; x <= radius; x += inc) {
        y = Math.sqrt(radius * radius - x * x);
        coordinates.push(x + radius);
        coordinates.push(y + radius);
    }
    for (let x = -radius; x <= radius; x += inc) {
        y = -Math.sqrt(radius * radius - x * x);
        coordinates.push(-x + radius);
        coordinates.push(y + radius);
    }
    return coordinates;
}

Crafty.c("wall", {
    required: "2D, Canvas, Color, Collision",
    init: function() {
        this.color(colors["wall"]);
    }
})

Crafty.c("ball", {
    required: "2D, Canvas, Color, Collision, Motion",
    init: function() {
        this.collision(getCircle(ballRadius))
    },
    events: {
        "Draw": function(data) {
            let ctx = data.ctx, pos = data.pos;
            ctx.beginPath(pos._x + ballRadius);
            ctx.arc(pos._x + ballRadius, pos._y + ballRadius, ballRadius, 0, 2 * Math.PI);
            ctx.fillStyle = colors["ball"]
            ctx.fill();
        },
        "ExitFrame": function(evt) {
            let hitDatas, hitData;
            Crafty.e('2D, Canvas, Color')
                .attr({
                    x: this.x + ballRadius,
                    y: this.y + ballRadius,
                    w: 3,
                    h: 3
                })
                .color('black')
            if (hitDatas = this.hit('Wall')) {
                hitData = hitDatas[0];
                this.x -= hitData.overlap * hitData.normal.x;
                this.y -= hitData.overlap * hitData.normal.y;
                let len_normal_vec = Math.sqrt(hitData.normal.x * hitData.normal.x + hitData.normal.y * hitData.normal.y);
                let cos_angle = hitData.normal.x / len_normal_vec;
                let angle_ball = Math.acos(this.vx / Math.sqrt(this.vx * this.vx + this.vy * this.vy)) * 180.0 / Math.PI;
                let angleA = Math.acos(cos_angle) * 180.0 / Math.PI;
                let delta = (angleA - 180 + angle_ball) * 2;
                let rx, ry;
                if (this.vy < 0 && hitData.normal.y >= 0)
                {
                    delta *= -1;
                }
                if (this.vx < 0 && this.vy > 0 && hitData.normal.x > 0 && hitData.normal.y > 0)
                {
                    delta *= -1;
                }
                if (this.vx < 0 && this.vy < 0 && hitData.normal.x > 0 && hitData.normal.y < 0)
                {
                    delta = 180 - delta;
                }
                if (this.vx > 0 && this.vy < 0 && hitData.normal.x < 0 && hitData.normal.y < 0)
                {
                    delta = 2 * angleA - 360 - 2 * angle_ball;
                }
                if (this.vx > 0 && this.vy > 0 && hitData.normal.x < 0 && hitData.normal.y > 0)
                {
                    delta = 180 + delta;
                }
                this.vx *= -1;
                this.vy *= -1;
                delta *= -1;
                rx = this.vx * Math.cos(delta * Math.PI / 180) - this.vy * Math.sin(delta * Math.PI / 180);
                ry = this.vx * Math.sin(delta * Math.PI / 180) + this.vy * Math.cos(delta * Math.PI / 180);
                this.vx = rx;
                this.vy = ry;
            }
        }
    }
})

Crafty.defineScene("main", function() {
    Crafty.init(gameWidth, gameHeight);
    Crafty.background(colors["background"]);

     let coordinatesOfWalls = [
        0, 0, 
        0, 0,
        0, gameHeight - wallWidth,
        gameWidth - wallWidth, 0
    ]
    let sizeOfWalls = [
        gameWidth, wallWidth,
        wallWidth, gameHeight,
        gameWidth, wallWidth,
        wallWidth, gameHeight
    ]
    for (let i = 0; i < 8; i += 2) {
        Crafty.e("wall, Wall")
            .attr({
                x: coordinatesOfWalls[i],
                y: coordinatesOfWalls[i + 1],
                w: sizeOfWalls[i],
                h: sizeOfWalls[i + 1]
            })
    }

    Crafty.e('2D, Canvas, Color, Collision, Wall') 
        .attr({
            x: 100,
            y: 200,
            w: 700,
            h: 700
        })
        .collision([
            0, 200,
            400, 0,
            600, 200,
            200, 400
        ])
        .bind("Draw", function(data) {
            let ctx = data.ctx, pos = data.pos;
            ctx.beginPath();
            ctx.lineTo(pos._x, pos._y + 200);
            ctx.lineTo(pos._x + 400, pos._y + 0);
            ctx.lineTo(pos._x + 600, pos._y + 200);
            ctx.lineTo(pos._x + 200, pos._y + 400);
            ctx.fillStyle = "blue";
            ctx.fill();
        })
    let ball = Crafty.e("ball, Ball")
        .attr({
            x: 50,
            y: 50,
            w: ballRadius * 2,
            h: ballRadius * 2
        })
        ball.vx = 200;
        ball.vy = -50;
})
